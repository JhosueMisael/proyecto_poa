package Modelo;

import java.util.Date;

/**
 *
 * @author Christian
 */
public class modelo_docente {
    private int pro_codigo;
    private String per_identificacion;
    private String per_primerapellido;
    private String per_segundoapellido; 
    private String per_primernombre;
    private String per_segundonombre;
    private String per_celular;
    private String per_correo; 
    private Date per_fechanacimiento;

    public modelo_docente() {
    }

    public modelo_docente(int pro_codigo, String per_identificacion, String per_primerapellido, String per_segundoapellido, String per_primernombre, String per_segundonombre, String per_celular, String per_correo, Date per_fechanacimiento) {
        this.pro_codigo = pro_codigo;
        this.per_identificacion = per_identificacion;
        this.per_primerapellido = per_primerapellido;
        this.per_segundoapellido = per_segundoapellido;
        this.per_primernombre = per_primernombre;
        this.per_segundonombre = per_segundonombre;
        this.per_celular = per_celular;
        this.per_correo = per_correo;
        this.per_fechanacimiento = per_fechanacimiento;
    }

    public int getPro_codigo() {
        return pro_codigo;
    }

    public void setPro_codigo(int pro_codigo) {
        this.pro_codigo = pro_codigo;
    }

    public String getPer_identificacion() {
        return per_identificacion;
    }

    public void setPer_identificacion(String per_identificacion) {
        this.per_identificacion = per_identificacion;
    }

    public String getPer_primerapellido() {
        return per_primerapellido;
    }

    public void setPer_primerapellido(String per_primerapellido) {
        this.per_primerapellido = per_primerapellido;
    }

    public String getPer_segundoapellido() {
        return per_segundoapellido;
    }

    public void setPer_segundoapellido(String per_segundoapellido) {
        this.per_segundoapellido = per_segundoapellido;
    }

    public String getPer_primernombre() {
        return per_primernombre;
    }

    public void setPer_primernombre(String per_primernombre) {
        this.per_primernombre = per_primernombre;
    }

    public String getPer_segundonombre() {
        return per_segundonombre;
    }

    public void setPer_segundonombre(String per_segundonombre) {
        this.per_segundonombre = per_segundonombre;
    }

    public String getPer_celular() {
        return per_celular;
    }

    public void setPer_celular(String per_celular) {
        this.per_celular = per_celular;
    }

    public String getPer_correo() {
        return per_correo;
    }

    public void setPer_correo(String per_correo) {
        this.per_correo = per_correo;
    }

    public Date getPer_fechanacimiento() {
        return per_fechanacimiento;
    }

    public void setPer_fechanacimiento(Date per_fechanacimiento) {
        this.per_fechanacimiento = per_fechanacimiento;
    }

    @Override
    public String toString() {
        return "modelo_docente{" + "pro_codigo=" + pro_codigo + ", per_identificacion=" + per_identificacion + ", per_primerapellido=" + per_primerapellido + ", per_segundoapellido=" + per_segundoapellido + ", per_primernombre=" + per_primernombre + ", per_segundonombre=" + per_segundonombre + ", per_celular=" + per_celular + ", per_correo=" + per_correo + ", per_fechanacimiento=" + per_fechanacimiento + '}';
    }
    
    public String Nombre_Apellido(){
        return per_primerapellido+" "+per_primernombre;
    }
}
