package Modelo;

/**
 *
 * @author Robben10
 */
public class actividad_responsable {

    private int id_activdad_responsables;
    private int profesor_cod;
    private String profesor_cedula;
    private int id_actividad;

    public actividad_responsable() {
    }

    public int getId_activdad_responsables() {
        return id_activdad_responsables;
    }

    public void setId_activdad_responsables(int id_activdad_responsables) {
        this.id_activdad_responsables = id_activdad_responsables;
    }

    public int getProfesor_cod() {
        return profesor_cod;
    }

    public void setProfesor_cod(int profesor_cod) {
        this.profesor_cod = profesor_cod;
    }

    public String getProfesor_cedula() {
        return profesor_cedula;
    }

    public void setProfesor_cedula(String profesor_cedula) {
        this.profesor_cedula = profesor_cedula;
    }

    public int getId_actividad() {
        return id_actividad;
    }

    public void setId_actividad(int id_actividad) {
        this.id_actividad = id_actividad;
    }

    
    
}
