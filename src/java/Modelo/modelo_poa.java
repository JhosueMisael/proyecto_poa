package Modelo;
import java.util.Date;
/**
 *
 * @author Christian
 */
public class modelo_poa {
    private int id_poa;
    private String des_poa;
    private Date fechi_poa;
    private Date fechf_poa;
    private boolean estado;

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public modelo_poa() {
    }

   
    public int getId_poa() {
        return id_poa;
    }

    public void setId_poa(int id_poa) {
        this.id_poa = id_poa;
    }

    public String getDes_poa() {
        return des_poa;
    }

    public void setDes_poa(String des_poa) {
        this.des_poa = des_poa;
    }

    public Date getFechi_poa() {
        return fechi_poa;
    }

    public void setFechi_poa(Date fechi_poa) {
        this.fechi_poa = fechi_poa;
    }

    public Date getFechf_poa() {
        return fechf_poa;
    }

    public void setFechf_poa(Date fechf_poa) {
        this.fechf_poa = fechf_poa;
    }

   
}
