package Modelo;

import java.util.Date;

/**
 *
 * @author Christian
 */
public class modelo_tarea {
    private int id_tarea;
    private String des_tarea;
    private Date fechi_tarea;
    private Date fechf_tarea;
    private String profesor_cod;
    private String profesor_nombre;
    private String profesor_apellido;
    private double ejec_tarea;
    private double cost_tarea;
    private String horas_tarea;
    private double total_acum_tarea;
    private boolean estado_tarea;
    private int id_secpoa;
    private String des_secpoa;

    public modelo_tarea() {
    }

    public modelo_tarea(int id_tarea, String des_tarea, Date fechi_tarea, Date fechf_tarea, String profesor_cod, String profesor_nombre, String profesor_apellido, double ejec_tarea, double cost_tarea, String horas_tarea, double total_acum_tarea, boolean estado_tarea, int id_secpoa, String des_secpoa) {
        this.id_tarea = id_tarea;
        this.des_tarea = des_tarea;
        this.fechi_tarea = fechi_tarea;
        this.fechf_tarea = fechf_tarea;
        this.profesor_cod = profesor_cod;
        this.profesor_nombre = profesor_nombre;
        this.profesor_apellido = profesor_apellido;
        this.ejec_tarea = ejec_tarea;
        this.cost_tarea = cost_tarea;
        this.horas_tarea = horas_tarea;
        this.total_acum_tarea = total_acum_tarea;
        this.estado_tarea = estado_tarea;
        this.id_secpoa = id_secpoa;
        this.des_secpoa = des_secpoa;
    }

    public int getId_tarea() {
        return id_tarea;
    }

    public void setId_tarea(int id_tarea) {
        this.id_tarea = id_tarea;
    }

    public String getDes_tarea() {
        return des_tarea;
    }

    public void setDes_tarea(String des_tarea) {
        this.des_tarea = des_tarea;
    }

    public Date getFechi_tarea() {
        return fechi_tarea;
    }

    public void setFechi_tarea(Date fechi_tarea) {
        this.fechi_tarea = fechi_tarea;
    }

    public Date getFechf_tarea() {
        return fechf_tarea;
    }

    public void setFechf_tarea(Date fechf_tarea) {
        this.fechf_tarea = fechf_tarea;
    }

    public String getProfesor_cod() {
        return profesor_cod;
    }

    public void setProfesor_cod(String profesor_cod) {
        this.profesor_cod = profesor_cod;
    }

    public String getProfesor_nombre() {
        return profesor_nombre;
    }

    public void setProfesor_nombre(String profesor_nombre) {
        this.profesor_nombre = profesor_nombre;
    }

    public String getProfesor_apellido() {
        return profesor_apellido;
    }

    public void setProfesor_apellido(String profesor_apellido) {
        this.profesor_apellido = profesor_apellido;
    }

    public double getEjec_tarea() {
        return ejec_tarea;
    }

    public void setEjec_tarea(double ejec_tarea) {
        this.ejec_tarea = ejec_tarea;
    }

    public double getCost_tarea() {
        return cost_tarea;
    }

    public void setCost_tarea(double cost_tarea) {
        this.cost_tarea = cost_tarea;
    }

    public String getHoras_tarea() {
        return horas_tarea;
    }

    public void setHoras_tarea(String horas_tarea) {
        this.horas_tarea = horas_tarea;
    }

    public double getTotal_acum_tarea() {
        return total_acum_tarea;
    }

    public void setTotal_acum_tarea(double total_acum_tarea) {
        this.total_acum_tarea = total_acum_tarea;
    }

    public boolean isEstado_tarea() {
        return estado_tarea;
    }

    public void setEstado_tarea(boolean estado_tarea) {
        this.estado_tarea = estado_tarea;
    }

    public int getId_secpoa() {
        return id_secpoa;
    }

    public void setId_secpoa(int id_secpoa) {
        this.id_secpoa = id_secpoa;
    }

    public String getDes_secpoa() {
        return des_secpoa;
    }

    public void setDes_secpoa(String des_secpoa) {
        this.des_secpoa = des_secpoa;
    }

    @Override
    public String toString() {
        return "modelo_tarea{" + "id_tarea=" + id_tarea + ", des_tarea=" + des_tarea + ", fechi_tarea=" + fechi_tarea + ", fechf_tarea=" + fechf_tarea + ", profesor_cod=" + profesor_cod + ", profesor_nombre=" + profesor_nombre + ", profesor_apellido=" + profesor_apellido + ", ejec_tarea=" + ejec_tarea + ", cost_tarea=" + cost_tarea + ", horas_tarea=" + horas_tarea + ", total_acum_tarea=" + total_acum_tarea + ", estado_tarea=" + estado_tarea + ", id_secpoa=" + id_secpoa + ", des_secpoa=" + des_secpoa + '}';
    }

    public String Nombre_Apellido(){
        return profesor_apellido+" "+profesor_nombre;
    }
}
