package Modelo;
/**
 *
 * @author Christian
 */
public class modelo_prioridad {
    private int id_prioridad;
    private String des_prioridad;

    public modelo_prioridad() {
    }

    public modelo_prioridad(int id_prioridad, String des_prioridad) {
        this.id_prioridad = id_prioridad;
        this.des_prioridad = des_prioridad;
    }

    public int getId_prioridad() {
        return id_prioridad;
    }

    public void setId_prioridad(int id_prioridad) {
        this.id_prioridad = id_prioridad;
    }

    public String getDes_prioridad() {
        return des_prioridad;
    }

    public void setDes_prioridad(String des_prioridad) {
        this.des_prioridad = des_prioridad;
    }

    @Override
    public String toString() {
        return "modelo_prioridad{" + "id_prioridad=" + id_prioridad + ", des_prioridad=" + des_prioridad + '}';
    }
    
    
}
