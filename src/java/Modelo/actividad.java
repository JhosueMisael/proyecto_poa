package Modelo;

import java.util.Date;

/**
 *
 * @author Robben10
 */
public class actividad {

    private int id_actividad;
    private String des_actividad;
    private Date fechi_actividad;
    private Date fechf_actividad;
    private double ejec_actividad;
    private double cost_actividad;
    private int hora_est_actividad;
    private double total_acum_actividad;
    private int id_prioridad;
    private int id_actividad_responsables;
    private int id_tarea;
    private boolean estado_actividad;

    public actividad() {
    }

    public int getId_actividad() {
        return id_actividad;
    }

    public void setId_actividad(int id_actividad) {
        this.id_actividad = id_actividad;
    }

    public String getDes_actividad() {
        return des_actividad;
    }

    public void setDes_actividad(String des_actividad) {
        this.des_actividad = des_actividad;
    }

    public Date getFechi_actividad() {
        return fechi_actividad;
    }

    public void setFechi_actividad(Date fechi_actividad) {
        this.fechi_actividad = fechi_actividad;
    }

    public Date getFechf_actividad() {
        return fechf_actividad;
    }

    public void setFechf_actividad(Date fechf_actividad) {
        this.fechf_actividad = fechf_actividad;
    }

    public double getEjec_actividad() {
        return ejec_actividad;
    }

    public void setEjec_actividad(double ejec_actividad) {
        this.ejec_actividad = ejec_actividad;
    }

    public double getCost_actividad() {
        return cost_actividad;
    }

    public void setCost_actividad(double cost_actividad) {
        this.cost_actividad = cost_actividad;
    }

    public int getHora_est_actividad() {
        return hora_est_actividad;
    }

    public void setHora_est_actividad(int hora_est_actividad) {
        this.hora_est_actividad = hora_est_actividad;
    }

    public double getTotal_acum_actividad() {
        return total_acum_actividad;
    }

    public void setTotal_acum_actividad(double total_acum_actividad) {
        this.total_acum_actividad = total_acum_actividad;
    }

    public int getId_prioridad() {
        return id_prioridad;
    }

    public void setId_prioridad(int id_prioridad) {
        this.id_prioridad = id_prioridad;
    }

    public int getId_actividad_responsables() {
        return id_actividad_responsables;
    }

    public void setId_actividad_responsables(int id_actividad_responsables) {
        this.id_actividad_responsables = id_actividad_responsables;
    }

    public int getId_tarea() {
        return id_tarea;
    }

    public void setId_tarea(int id_tarea) {
        this.id_tarea = id_tarea;
    }

    public boolean isEstado_actividad() {
        return estado_actividad;
    }

    public void setEstado_actividad(boolean estado_actividad) {
        this.estado_actividad = estado_actividad;
    }

    @Override
    public String toString() {
        return "actividad{" + "id_actividad=" + id_actividad + ", des_actividad=" + des_actividad + ", fechi_actividad=" + fechi_actividad + ", fechf_actividad=" + fechf_actividad + ", ejec_actividad=" + ejec_actividad + ", cost_actividad=" + cost_actividad + ", hora_est_actividad=" + hora_est_actividad + ", total_acum_actividad=" + total_acum_actividad + ", id_prioridad=" + id_prioridad + ", id_actividad_responsables=" + id_actividad_responsables + ", id_tarea=" + id_tarea + ", estado_actividad=" + estado_actividad + '}';
    }

}
