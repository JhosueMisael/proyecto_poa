package Modelo;
/**
 *
 * @author Christian
 */
public class modelo_seccion_poa {
    private int id_secpoa;
    private String des_secpoa;
    private boolean estado;
    private int id_poa;
    private String des_poa;

    public modelo_seccion_poa() {
    }

    public int getId_secpoa() {
        return id_secpoa;
    }

    public void setId_secpoa(int id_secpoa) {
        this.id_secpoa = id_secpoa;
    }

    public String getDes_secpoa() {
        return des_secpoa;
    }

    public void setDes_secpoa(String des_secpoa) {
        this.des_secpoa = des_secpoa;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

   

    public int getId_poa() {
        return id_poa;
    }

    public void setId_poa(int id_poa) {
        this.id_poa = id_poa;
    }

    public String getDes_poa() {
        return des_poa;
    }

    public void setDes_poa(String des_poa) {
        this.des_poa = des_poa;
    }

    @Override
    public String toString() {
        return "modelo_seccion_poa{" + "id_secpoa=" + id_secpoa + ", des_secpoa=" + des_secpoa + ", estado=" + estado + ", id_poa=" + id_poa + ", des_poa=" + des_poa + '}';
    }

  
    
    
}
