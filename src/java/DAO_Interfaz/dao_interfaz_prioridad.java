
package DAO_Interfaz;

import Modelo.modelo_prioridad;
import java.util.List;

/**
 *
 * @author Christian
 */
public interface dao_interfaz_prioridad {
    public void Guardar(modelo_prioridad prioridad) throws Exception;
    public void Actualizar(modelo_prioridad prioridad) throws Exception;
    public void Eliminar(modelo_prioridad prioridad) throws Exception;
    public List <modelo_prioridad> Lista_Prioridad() throws Exception;
}
