package DAO_Interfaz;

import java.util.List;
import Modelo.actividad;

/**
 *
 * @author Robben10
 */
public interface dao_actividad_interf {

    public void Guardar(actividad acti) throws Exception;
    public void Actualizar(actividad acti) throws Exception;
    public void Eliminar(actividad acti) throws Exception;
    public List<actividad> lista_actividad() throws Exception;

}
