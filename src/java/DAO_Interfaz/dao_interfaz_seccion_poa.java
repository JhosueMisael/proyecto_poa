
package DAO_Interfaz;

import Modelo.modelo_seccion_poa;
import java.util.List;

/**
 *
 * @author Christian
 */
public interface dao_interfaz_seccion_poa {
    public void Guardar(modelo_seccion_poa seccion_poa) throws Exception;
    public void Actualizar(modelo_seccion_poa seccion_poa) throws Exception;
    public void Eliminar(modelo_seccion_poa seccion_poa) throws Exception;
    public List <modelo_seccion_poa> Lista_Seccion_POA() throws Exception;
}
