
package DAO_Interfaz;

import java.util.List;
import Modelo.modelo_poa;
/**
 *
 * @author Christian
 */
public interface dao_interfaz_poa {
    public void Guardar(modelo_poa poa) throws Exception;
    public void Actualizar(modelo_poa poa) throws Exception;
    public void Eliminar(modelo_poa poa) throws Exception;
    public List <modelo_poa> Lista_POA() throws Exception;
}
