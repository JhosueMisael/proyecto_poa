package DAO_Interfaz;

import java.util.List;
import Modelo.actividad_responsable;

/**
 *
 * @author Robben10
 */
public interface dao_actividad_respo_interf {

    public void Guardar(actividad_responsable acti_resp) throws Exception;
    public void Actualizar(actividad_responsable acti_resp) throws Exception;
    public void Eliminar(actividad_responsable acti_resp) throws Exception;
    public List<actividad_responsable> lista_actividad_respo() throws Exception;

}
