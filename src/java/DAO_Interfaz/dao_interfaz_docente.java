package DAO_Interfaz;
import Modelo.modelo_docente;
import java.util.List;

/**
 *
 * @author Christian
 */
public interface dao_interfaz_docente {
    public void Guardar(modelo_docente docente) throws Exception;
    public void Actualizar(modelo_docente docente) throws Exception;
    public void Eliminar(modelo_docente docente) throws Exception;
    public List <modelo_docente> Lista_Docente() throws Exception;
}
