
package DAO_Interfaz;

import Modelo.modelo_tarea;
import java.util.List;

/**
 *
 * @author Christian
 */
public interface dao_interfaz_tarea {
    public void Guardar(modelo_tarea tarea) throws Exception;
    public void Actualizar(modelo_tarea tarea) throws Exception;
    public void Eliminar(modelo_tarea tarea) throws Exception;
    public List <modelo_tarea> Lista_Tarea() throws Exception;
}
