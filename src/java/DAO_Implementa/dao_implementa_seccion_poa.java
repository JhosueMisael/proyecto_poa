package DAO_Implementa;
import DAO_Interfaz.dao_interfaz_seccion_poa;
import Conexion.conexion_bd_pg;
import Modelo.modelo_seccion_poa;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Christian
 */
public class dao_implementa_seccion_poa extends conexion_bd_pg implements dao_interfaz_seccion_poa{

    @Override
    public void Guardar(modelo_seccion_poa seccion_poa) throws Exception {
        try {
            String sql="INSERT INTO public.seccion_poa(\n" +
"	des_secpoa, estado_secpoa, id_poa)\n" +
"	VALUES (?, ?, ?);";
            this.conectar();
            PreparedStatement st=this.conexion.prepareStatement(sql);
            st.setString(1, seccion_poa.getDes_secpoa());
            st.setBoolean(2, seccion_poa.isEstado());
            st.setInt(3, seccion_poa.getId_poa());
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally{
            this.cerrar();
        }
            
    }

    @Override
    public void Actualizar(modelo_seccion_poa seccion_poa) throws Exception {
        try {
            String sql="UPDATE public.seccion_poa\n" +
"	SET des_secpoa=?, estado=?, id_poa=?\n" +
"	WHERE seccion_poa.id_secpoa='"+seccion_poa.getId_secpoa()+"';";
            this.conectar();
            PreparedStatement st= this.conexion.prepareStatement(sql);
            st.setString(1, seccion_poa.getDes_secpoa());
            st.setBoolean(2, seccion_poa.isEstado());
            st.setInt(3, seccion_poa.getId_poa());
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally{
            this.cerrar();
        }
    }

    @Override
    public void Eliminar(modelo_seccion_poa seccion_poa) throws Exception {
        try {
            String sql="DELETE FROM public.seccion_poa\n" +
"	WHERE seccion_poa='"+seccion_poa.getId_secpoa()+"';";
            this.conectar();
            PreparedStatement st=this.conexion.prepareStatement(sql);
            st.setInt(1, seccion_poa.getId_secpoa());
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public List<modelo_seccion_poa> Lista_Seccion_POA() throws Exception {
        List<modelo_seccion_poa> lista_seccion_poa=null;
        String sql="SELECT id_secpoa, des_secpoa, seccion_poa.estado_secpoa ,seccion_poa.id_poa , poa.des_poa\n" +
"	FROM public.seccion_poa,public.poa where poa.id_poa=seccion_poa.id_poa;";
        try {
            this.conectar();
            PreparedStatement st=this.conexion.prepareStatement(sql);
            ResultSet rs=st.executeQuery();
            lista_seccion_poa=new ArrayList<>();
            while (rs.next()) {                
                modelo_seccion_poa obj_seccion_poa=new modelo_seccion_poa();
                obj_seccion_poa.setId_secpoa(rs.getInt(1));
                obj_seccion_poa.setDes_secpoa(rs.getString(2));
                obj_seccion_poa.setEstado(rs.getBoolean(3));
                obj_seccion_poa.setId_poa(rs.getInt(4));
                obj_seccion_poa.setDes_poa(rs.getString(5));
                lista_seccion_poa.add(obj_seccion_poa);
            }
            rs.close();
            st.close();
        } catch (Exception e) {
            throw e;
        } finally{
            this.cerrar();
        }
        return lista_seccion_poa;
    } 
}
