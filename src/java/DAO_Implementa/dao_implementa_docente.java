package DAO_Implementa;
import DAO_Interfaz.dao_interfaz_docente;
import Conexion.conexion_bd_insta;
import Modelo.modelo_docente;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Christian
 */
public class dao_implementa_docente extends conexion_bd_insta implements dao_interfaz_docente{

    @Override
    public void Guardar(modelo_docente docente) throws Exception {
         try {
            String sql = "";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.setInt(1, docente.getPro_codigo());
            st.setString(2, docente.getPer_identificacion());
            st.setString(3, docente.getPer_primerapellido());
            st.setString(4, docente.getPer_segundoapellido());
            st.setString(5, docente.getPer_primernombre());
            st.setString(6, docente.getPer_segundonombre());
            st.setString(7, docente.getPer_celular());
            st.setString(8, docente.getPer_correo());
            st.setDate(9, new Date(docente.getPer_fechanacimiento().getTime()));
            
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public void Actualizar(modelo_docente docente) throws Exception {
        try {
            String sql = "";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.setString(2, docente.getPer_identificacion());
            st.setString(3, docente.getPer_primerapellido());
            st.setString(4, docente.getPer_segundoapellido());
            st.setString(5, docente.getPer_primernombre());
            st.setString(6, docente.getPer_segundonombre());
            st.setString(7, docente.getPer_celular());
            st.setString(8, docente.getPer_correo());
            st.setDate(9, new Date(docente.getPer_fechanacimiento().getTime()));
            
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public void Eliminar(modelo_docente docente) throws Exception {
        try {
            String sql = "";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public List<modelo_docente> Lista_Docente() throws Exception {
        List<modelo_docente> lista_docentes = null;
        String sql = "SELECT pro_codigo, per_identificacion, per_primerapellido, per_segundoapellido, per_primernombre, per_segundonombre, per_celular, per_correo, per_fechanacimiento\n" +
"	FROM public.vdocentes_ed order by per_primerapellido asc;";
        try {
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            lista_docentes = new ArrayList();
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                modelo_docente obj_docente = new modelo_docente();
                obj_docente.setPro_codigo(rs.getInt(1));
                obj_docente.setPer_identificacion(rs.getString(2));
                obj_docente.setPer_primerapellido(rs.getString(3));
                obj_docente.setPer_segundoapellido(rs.getString(4));
                obj_docente.setPer_primernombre(rs.getString(5));
                obj_docente.setPer_segundonombre(rs.getString(6));
                obj_docente.setPer_celular(rs.getString(7));
                obj_docente.setPer_correo(rs.getString(8));
                obj_docente.setPer_fechanacimiento(rs.getDate(9));
                lista_docentes.add(obj_docente);
            }
            st.close();
            rs.close();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
        return lista_docentes;
    }
    
}
