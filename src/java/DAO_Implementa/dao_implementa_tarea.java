package DAO_Implementa;
import DAO_Interfaz.dao_interfaz_tarea;
import Conexion.conexion_bd_pg;
import Modelo.modelo_tarea;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Christian
 */
public class dao_implementa_tarea extends conexion_bd_pg implements dao_interfaz_tarea{

    @Override
    public void Guardar(modelo_tarea tarea) throws Exception {
        try {
            String sql = "INSERT INTO public.tarea(\n" +
"	des_tarea, fechi_tarea, fechf_tarea, profesor_cod, ejec_tarea, cost_tarea, horas_tarea, total_acum_tarea, id_secpoa, estado_tarea)\n" +
"	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.setString(1, tarea.getDes_tarea());
            st.setDate(2, new Date(tarea.getFechi_tarea().getTime()));
            st.setDate(3, new Date(tarea.getFechf_tarea().getTime()));
            st.setString(4, tarea.getProfesor_cod());
            st.setDouble(5, tarea.getEjec_tarea());
            st.setDouble(6, tarea.getCost_tarea());
            st.setString(7, tarea.getHoras_tarea());
            st.setDouble(8, tarea.getTotal_acum_tarea());
            st.setInt(9, tarea.getId_secpoa());
            st.setBoolean(10, tarea.isEstado_tarea());
            
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public void Actualizar(modelo_tarea tarea) throws Exception {
        try {
            String sql = "UPDATE public.tarea\n" +
"	SET des_tarea=?, fechi_tarea=?, fechf_tarea=?, profesor_cod=?, ejec_tarea=?, cost_tarea=?, horas_tarea=?, total_acum_tarea=?, id_secpoa=?, estado_tarea=?\n" +
"	WHERE tarea.id_tarea='"+tarea.getId_tarea()+"';";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.setString(1, tarea.getDes_tarea());
            st.setDate(2, new Date(tarea.getFechi_tarea().getTime()));
            st.setDate(3, new Date(tarea.getFechf_tarea().getTime()));
            st.setString(4, tarea.getProfesor_cod());
            st.setDouble(5, tarea.getEjec_tarea());
            st.setDouble(6, tarea.getCost_tarea());
            st.setString(7, tarea.getHoras_tarea());
            st.setDouble(8, tarea.getTotal_acum_tarea());
            st.setInt(9, tarea.getId_secpoa());
            st.setBoolean(10, tarea.isEstado_tarea());
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public void Eliminar(modelo_tarea tarea) throws Exception {
         try {
            String sql = "DELETE FROM public.tarea\n" +
"	WHERE tarea.id_tarea='"+tarea.getId_tarea()+"';";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public List<modelo_tarea> Lista_Tarea() throws Exception {
        List<modelo_tarea> lista_tarea = null;
        String sql = "SELECT id_tarea, des_tarea, fechi_tarea, fechf_tarea, profesor_cod, ejec_tarea, cost_tarea, horas_tarea, total_acum_tarea, tarea.id_secpoa,seccion_poa.des_secpoa, estado_tarea\n" +
"	FROM public.tarea,public.seccion_poa where tarea.id_secpoa=seccion_poa.id_secpoa;";
        try {
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            lista_tarea = new ArrayList();
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                modelo_tarea obj_tarea = new modelo_tarea();
                obj_tarea.setId_tarea(rs.getInt(1));
                obj_tarea.setDes_tarea(rs.getString(2));
                obj_tarea.setFechi_tarea(rs.getDate(3));
                obj_tarea.setFechf_tarea(rs.getDate(4));
                obj_tarea.setProfesor_cod(rs.getString(5));
                obj_tarea.setEjec_tarea(rs.getDouble(6));
                obj_tarea.setCost_tarea(rs.getDouble(7));
                obj_tarea.setHoras_tarea(rs.getString(8));
                obj_tarea.setTotal_acum_tarea(rs.getDouble(9));
                obj_tarea.setId_secpoa(rs.getInt(10));
                obj_tarea.setDes_secpoa(rs.getString(11));
                obj_tarea.setEstado_tarea(rs.getBoolean(12));
                lista_tarea.add(obj_tarea);
            }
            st.close();
            rs.close();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
        return lista_tarea;
    }
    
}
