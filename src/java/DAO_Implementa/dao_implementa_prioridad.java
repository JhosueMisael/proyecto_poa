
package DAO_Implementa;
import DAO_Interfaz.dao_interfaz_prioridad;
import Conexion.conexion_bd_pg;
import Modelo.modelo_prioridad;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Christian
 */
public class dao_implementa_prioridad extends conexion_bd_pg implements dao_interfaz_prioridad{

    @Override
    public void Guardar(modelo_prioridad prioridad) throws Exception {
        try {
            String sql = "INSERT INTO public.prioridad(\n" +
"	des_prioridad)\n" +
"	VALUES (?);";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.setString(1, prioridad.getDes_prioridad());
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public void Actualizar(modelo_prioridad prioridad) throws Exception {
        try {
            String sql = "UPDATE public.prioridad\n" +
"	SET des_prioridad=?\n" +
"	WHERE id_prioridad='"+prioridad.getId_prioridad()+"';";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.setString(1, prioridad.getDes_prioridad());
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public void Eliminar(modelo_prioridad prioridad) throws Exception {
        try {
            String sql = "DELETE FROM public.prioridad\n" +
"	WHERE id_prioridad='"+prioridad.getId_prioridad()+"';";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public List<modelo_prioridad> Lista_Prioridad() throws Exception {
        List<modelo_prioridad> lista_prioridad = null;
        String sql = "SELECT id_prioridad, des_prioridad\n" +
"	FROM public.prioridad;";
        try {
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            lista_prioridad = new ArrayList();
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                modelo_prioridad obj_prioridad = new modelo_prioridad();
                obj_prioridad.setId_prioridad(rs.getInt(1));
                obj_prioridad.setDes_prioridad(rs.getString(2));
                lista_prioridad.add(obj_prioridad);
            }
            st.close();
            rs.close();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
        return lista_prioridad;
    }
    
}
