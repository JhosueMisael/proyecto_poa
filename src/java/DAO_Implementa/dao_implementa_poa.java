package DAO_Implementa;
import DAO_Interfaz.dao_interfaz_poa;
import Conexion.conexion_bd_pg;
import Modelo.modelo_poa;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Christian
 */
public class dao_implementa_poa extends conexion_bd_pg implements dao_interfaz_poa{

    @Override
    public void Guardar(modelo_poa poa) throws Exception {
        try {
            String sql = "INSERT INTO public.poa(\n" +
"	des_poa, fechi_poa, fechf_poa, estado)\n" +
"	VALUES (?, ?, ?, ?);";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.setString(1, poa.getDes_poa());
            st.setDate(2, new Date(poa.getFechi_poa().getTime()));
            st.setDate(3, new Date(poa.getFechf_poa().getTime()));
            st.setBoolean(4, poa.isEstado());
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public void Actualizar(modelo_poa poa) throws Exception {
         try {
            String sql = "UPDATE public.poa\n" +
"	SET des_poa=?, fechi_poa=?, fechf_poa=?, estado=?\n" +
"	WHERE id_poa='"+poa.getId_poa()+"';";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.setString(1, poa.getDes_poa());
            st.setDate(2, new Date(poa.getFechi_poa().getTime()));
            st.setDate(3, new Date(poa.getFechf_poa().getTime()));
            st.setBoolean(4, poa.isEstado());
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public void Eliminar(modelo_poa poa) throws Exception {
        try {
            String sql = "DELETE FROM public.poa\n" +
"	WHERE poa.id_poa='"+poa.getId_poa()+"';";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            
            st.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public List<modelo_poa> Lista_POA() throws Exception {
        List<modelo_poa> lista_poa = null;
        String sql = "SELECT id_poa, des_poa, fechi_poa, fechf_poa, estado\n" +
"	FROM public.poa;";
        try {
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            lista_poa = new ArrayList();
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                modelo_poa obj_poa = new modelo_poa();
                obj_poa.setId_poa(rs.getInt(1));
                obj_poa.setDes_poa(rs.getString(2));
                obj_poa.setFechi_poa(rs.getDate(3));
                obj_poa.setFechf_poa(rs.getDate(4));
                obj_poa.setEstado(rs.getBoolean(5));
                lista_poa.add(obj_poa);
            }
            st.close();
            rs.close();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
        return lista_poa;
    }
    
}
