package DAO_Implementa;

import Conexion.conexion_bd_pg;
import DAO_Interfaz.dao_actividad_interf;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import Modelo.actividad;

/**
 *
 * @author Robben10
 */
public class dao_actividad_imple extends conexion_bd_pg implements dao_actividad_interf {

    @Override
    public void Guardar(actividad acti) throws Exception {

        try {
//            FacesMessage msg = null;
            String sql = "INSERT INTO actividad("
                    + "des_actividad, fechi_actividad, fechf_actividad, ejec_actividad"
                    + ", cost_actividad, hora_est_actividad, total_acum_actividad, estado_actividad)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.setString(1, acti.getDes_actividad());
            st.setDate(2, new Date(acti.getFechi_actividad().getTime()));
            st.setDate(3, new Date(acti.getFechf_actividad().getTime()));
            st.setDouble(4, acti.getEjec_actividad());
            st.setDouble(5, acti.getCost_actividad());
            st.setInt(6, acti.getHora_est_actividad());
            st.setDouble(7, acti.getTotal_acum_actividad());
            st.setBoolean(8, acti.isEstado_actividad());
            st.executeUpdate();
//            msg = new FacesMessage("SAVE SUCESSFULLY");
//            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }

    }

    @Override
    public void Actualizar(actividad acti) throws Exception {
        try {
//            FacesMessage msg = null;
            String sql = "UPDATE actividad"
                    + "	SET des_actividad=?, fechi_actividad=?, fechf_actividad=?, ejec_actividad=?, "
                    + "cost_actividad=?, hora_est_actividad=?, total_acum_actividad=?, estado_actividad=?"
                    + "	WHERE id_actividad=" + acti.getId_actividad() + ";";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.setString(1, acti.getDes_actividad());
            st.setDate(2, new Date(acti.getFechi_actividad().getTime()));
            st.setDate(3, new Date(acti.getFechf_actividad().getTime()));
            st.setDouble(4, acti.getEjec_actividad());
            st.setDouble(5, acti.getCost_actividad());
            st.setInt(6, acti.getHora_est_actividad());
            st.setDouble(7, acti.getTotal_acum_actividad());
            st.setBoolean(8, acti.isEstado_actividad());
            st.executeUpdate();
//            msg = new FacesMessage("UPDATE SUCESSFULLY");
//            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public void Eliminar(actividad acti) throws Exception {

        try {
//            FacesMessage msg = null;
            String sql = "DELETE FROM actividad WHERE id_actividad=?;";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.setInt(1, acti.getId_actividad());
            st.executeUpdate();
//            msg = new FacesMessage("DELETE SUCESSFULLY");
//            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }

    }

    @Override
    public List<actividad> lista_actividad() throws Exception {

        List<actividad> lista_actividad = null;
        String sql = "SELECT * FROM actividad;";
        try {
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            lista_actividad = new ArrayList();
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                actividad obj_actividad = new actividad();
                obj_actividad.setId_actividad(rs.getInt(1));
                obj_actividad.setDes_actividad(rs.getString(2));
                obj_actividad.setFechi_actividad(rs.getDate(3));
                obj_actividad.setFechf_actividad(rs.getDate(4));
                obj_actividad.setEjec_actividad(rs.getDouble(5));
                obj_actividad.setCost_actividad(rs.getDouble(6));
                obj_actividad.setHora_est_actividad(rs.getInt(7));
                obj_actividad.setTotal_acum_actividad(rs.getInt(8));
                obj_actividad.setId_prioridad(rs.getInt(9));
                obj_actividad.setId_actividad_responsables(rs.getInt(10));
                obj_actividad.setId_tarea(rs.getInt(11));
                obj_actividad.setEstado_actividad(rs.getBoolean(12));
                lista_actividad.add(obj_actividad);

            }
            st.close();
            rs.close();
            System.out.println("*************************************************++" + lista_actividad.isEmpty());
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
        return lista_actividad;
    }

}
