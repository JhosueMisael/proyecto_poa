package DAO_Implementa;

import Conexion.conexion_bd_pg;
import DAO_Interfaz.dao_actividad_respo_interf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import Modelo.actividad_responsable;

/**
 *
 * @author Robben10
 */
public class dao_actividad_respo_imple extends conexion_bd_pg implements dao_actividad_respo_interf {

    @Override
    public void Guardar(actividad_responsable acti_resp) throws Exception {

        try {
//            FacesMessage msg = null;
            String sql = "INSERT INTO actividad_responsables(profesor_cod, profesor_cedula)"
                    + " VALUES (?, ?);";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.setInt(1, acti_resp.getProfesor_cod());
            st.setString(2, acti_resp.getProfesor_cedula());
            st.executeUpdate();
//            msg = new FacesMessage("SAVE SUCESSFULLY");
//            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }

    }

    @Override
    public void Actualizar(actividad_responsable acti_resp) throws Exception {

        try {
//            FacesMessage msg = null;
            String sql = "UPDATE actividad_responsables "
                    + "SET profesor_cod=?, profesor_cedula=?"
                    + "WHERE id_actividad_responsables=" + acti_resp.getId_activdad_responsables() + ";";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.setInt(1, acti_resp.getProfesor_cod());
            st.setString(2, acti_resp.getProfesor_cedula());
            st.executeUpdate();
//            msg = new FacesMessage("UPDATE SUCESSFULLY");
//            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
    }

    @Override
    public void Eliminar(actividad_responsable acti_resp) throws Exception {

        try {
//            FacesMessage msg = null;
            String sql = "DELETE FROM actividad_responsables "
                    + "WHERE id_actividad_responsables=?;";
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            st.setInt(1, acti_resp.getId_activdad_responsables());
            st.executeUpdate();
//            msg = new FacesMessage("DELETE SUCESSFULLY");
//            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }

    }

    @Override
    public List<actividad_responsable> lista_actividad_respo() throws Exception {
        List<actividad_responsable> lista_acti_respo = null;
        String sql = "SELECT * FROM actividad_responsables;";
        try {
            this.conectar();
            PreparedStatement st = this.conexion.prepareStatement(sql);
            lista_acti_respo = new ArrayList();
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                actividad_responsable obj_acti_respo = new actividad_responsable();
                obj_acti_respo.setId_activdad_responsables(rs.getInt(1));
                obj_acti_respo.setProfesor_cod(rs.getInt(2));
                obj_acti_respo.setProfesor_cedula(rs.getString(3));
                obj_acti_respo.setId_actividad(rs.getInt(4));
                lista_acti_respo.add(obj_acti_respo);
            }
            st.close();
            rs.close();
            System.out.println("**************************************" + lista_acti_respo.isEmpty());
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
        return lista_acti_respo;
    }

}
