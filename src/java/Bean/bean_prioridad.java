package Bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import Modelo.modelo_prioridad;
import DAO_Implementa.dao_implementa_prioridad;
import DAO_Interfaz.dao_interfaz_prioridad;
import java.util.ArrayList;
import java.util.List;
import org.primefaces.component.datatable.DataTable;
/**
 *
 * @author Christian
 */
@ManagedBean(name = "bean_prioridad")
@ViewScoped
public class bean_prioridad {

    private org.primefaces.component.datatable.DataTable tabla;
    
    modelo_prioridad obj_prioridad;
    
    List<modelo_prioridad> lista_prioridad=new ArrayList<>();
    
    dao_interfaz_prioridad obj_interfaz_prioridad=new dao_implementa_prioridad();
    
    boolean guardar=false;
    boolean actualizar=false;
    /**
     * Creates a new instance of NewJSFManagedBean
     */
    public bean_prioridad() throws Exception{
        obj_prioridad = new modelo_prioridad();
        lista_prioridad=obj_interfaz_prioridad.Lista_Prioridad();
        activa_botones(false, true);
    }
    
    public void guardar() throws Exception{ 
        obj_interfaz_prioridad.Guardar(obj_prioridad);
        lista_prioridad=obj_interfaz_prioridad.Lista_Prioridad();
        obj_prioridad=new modelo_prioridad();
        activa_botones(false, true);
    }
    public void actuaizar() throws Exception{
        obj_interfaz_prioridad.Actualizar(obj_prioridad);
        lista_prioridad=obj_interfaz_prioridad.Lista_Prioridad();
        obj_prioridad=new modelo_prioridad();
        activa_botones(false, true);
    }
    public void eliminar() throws Exception{
        
    }
    public void cargar_textos() throws Exception{
        obj_prioridad=new modelo_prioridad();
        obj_prioridad=(modelo_prioridad) tabla.getRowData();
        activa_botones(true, false);
    }
    
    public void activa_botones(boolean guardar,boolean actualizar){
        this.guardar=guardar;
        this.actualizar=actualizar;
    }

    public boolean isGuardar() {
        return guardar;
    }

    public void setGuardar(boolean guardar) {
        this.guardar = guardar;
    }

    public boolean isActualizar() {
        return actualizar;
    }

    public void setActualizar(boolean actualizar) {
        this.actualizar = actualizar;
    }
    
    

    public DataTable getTabla() {
        return tabla;
    }

    public void setTabla(DataTable tabla) {
        this.tabla = tabla;
    }

    public modelo_prioridad getObj_prioridad() {
        return obj_prioridad;
    }

    public void setObj_prioridad(modelo_prioridad obj_prioridad) {
        this.obj_prioridad = obj_prioridad;
    }

    public List<modelo_prioridad> getLista_prioridad() {
        return lista_prioridad;
    }

    public void setLista_prioridad(List<modelo_prioridad> lista_prioridad) {
        this.lista_prioridad = lista_prioridad;
    }

    public dao_interfaz_prioridad getObj_interfaz_prioridad() {
        return obj_interfaz_prioridad;
    }

    public void setObj_interfaz_prioridad(dao_interfaz_prioridad obj_interfaz_prioridad) {
        this.obj_interfaz_prioridad = obj_interfaz_prioridad;
    }
    
}
