package Bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import Modelo.modelo_poa;
import DAO_Interfaz.dao_interfaz_poa;
import DAO_Implementa.dao_implementa_poa;
import java.util.ArrayList;
import java.util.List;
import org.primefaces.component.datatable.DataTable;


/**
 *
 * @author Christian
 */
@ManagedBean(name = "bean_poa")
@ViewScoped
public class bean_poa {

    private org.primefaces.component.datatable.DataTable tabla;
    modelo_poa obj_poa;
    List<modelo_poa> lista_poa = new ArrayList<>();
    dao_interfaz_poa obj_dao_interfaz_poa = new dao_implementa_poa();

    boolean actualizar = false;
    boolean guardar=false;

    public bean_poa() throws Exception {
        lista_poa = obj_dao_interfaz_poa.Lista_POA();
        obj_poa = new modelo_poa();
        activa_botones(false, true);
    }

    
    public void activa_botones(boolean guardar, boolean actualizar){
        this.guardar=guardar;
        this.actualizar=actualizar;
    }
    
    public boolean isActualizar() {
        return actualizar;
    }

    public void setActualizar(boolean actualizar) {
        this.actualizar = actualizar;
    }

    public boolean isGuardar() {
        return guardar;
    }

    public void setGuardar(boolean guardar) {
        this.guardar = guardar;
    }

    public dao_interfaz_poa getObj_dao_interfaz_poa() {
        return obj_dao_interfaz_poa;
    }

    public void setObj_dao_interfaz_poa(dao_interfaz_poa obj_dao_interfaz_poa) {
        this.obj_dao_interfaz_poa = obj_dao_interfaz_poa;
    }

    public DataTable getTabla() {
        return tabla;
    }

    /**
     * Creates a new instance of bean_poa
     */
    public void setTabla(DataTable tabla) {
        this.tabla = tabla;
    }

    public modelo_poa getObj_poa() {
        return obj_poa;
    }

    public void setObj_poa(modelo_poa obj_poa) {
        this.obj_poa = obj_poa;
    }

    public List<modelo_poa> getLista_poa() {
        return lista_poa;
    }

    public void setLista_poa(List<modelo_poa> lista_poa) {
        this.lista_poa = lista_poa;
    }

    public void guardar() throws Exception {

        obj_dao_interfaz_poa.Guardar(obj_poa);
        lista_poa = obj_dao_interfaz_poa.Lista_POA();
        obj_poa = new modelo_poa();
        activa_botones(false, true);

    }

    public void actualizar() throws Exception {
        obj_dao_interfaz_poa.Actualizar(obj_poa);
        obj_poa = new modelo_poa();
        activa_botones(false, true);
    }

    public void eliminar() throws Exception {
        obj_poa=new modelo_poa();
        obj_poa = (modelo_poa) tabla.getRowData();
        obj_dao_interfaz_poa.Eliminar(obj_poa);
        lista_poa = obj_dao_interfaz_poa.Lista_POA();
        activa_botones(false, true);
    }

    public void cargar_textos() throws Exception {
        obj_poa = (modelo_poa) tabla.getRowData();
        activa_botones(true, false);
    }
}
