/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import Modelo.modelo_tarea;
import Modelo.modelo_seccion_poa;
import Modelo.modelo_docente;
import DAO_Implementa.dao_implementa_seccion_poa;
import DAO_Implementa.dao_implementa_tarea;
import DAO_Implementa.dao_implementa_docente;
import DAO_Interfaz.dao_interfaz_seccion_poa;
import DAO_Interfaz.dao_interfaz_tarea;
import DAO_Interfaz.dao_interfaz_docente;
import java.util.ArrayList;
import java.util.List;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author Christian
 */
@ManagedBean(name = "bean_tarea")
@ViewScoped
public class bean_tarea {
    private org.primefaces.component.datatable.DataTable tabla;
    
    modelo_tarea obj_tarea;
    modelo_seccion_poa obj_secpoa;
    modelo_docente obj_docentes;
    
    dao_interfaz_seccion_poa obj_interfaz_secpoa=new dao_implementa_seccion_poa();
    dao_interfaz_tarea obj_interfaz_tarea=new dao_implementa_tarea();
    dao_interfaz_docente obj_interfaz_docente=new dao_implementa_docente();
    
    List<modelo_seccion_poa> lista_secpoa=new ArrayList();
    List<modelo_tarea> lista_tarea=new ArrayList();
    List<modelo_docente> lista_docentes=new ArrayList<>();
    
    boolean guardar=false;
    boolean actualizar=false;
    
    /**
     * Creates a new instance of bean_tarea
     */
    public bean_tarea() throws Exception{
        obj_secpoa=new modelo_seccion_poa();
        obj_tarea=new modelo_tarea();
        obj_docentes=new modelo_docente();
        lista_tarea=obj_interfaz_tarea.Lista_Tarea();
        lista_secpoa=obj_interfaz_secpoa.Lista_Seccion_POA();
        lista_docentes=obj_interfaz_docente.Lista_Docente();
        activa_botones(false, true);
    }
    
    public void guardar() throws Exception{ 
        obj_tarea.setProfesor_cod(""+obj_docentes.getPro_codigo());
        obj_tarea.setId_secpoa(obj_secpoa.getId_secpoa());
        obj_interfaz_tarea.Guardar(obj_tarea);
        lista_tarea=obj_interfaz_tarea.Lista_Tarea();
        obj_tarea=new modelo_tarea();
        obj_docentes=new modelo_docente();
        obj_secpoa= new modelo_seccion_poa();
        activa_botones(false, true);
    }
    public void actuaizar() throws Exception{
        obj_tarea.setId_secpoa(obj_secpoa.getId_secpoa());
        obj_tarea.setProfesor_cod(""+obj_docentes.getPro_codigo());
        obj_interfaz_tarea.Actualizar(obj_tarea);
        obj_tarea=new modelo_tarea();
        obj_docentes=new modelo_docente();
        obj_secpoa= new modelo_seccion_poa();
        activa_botones(false, true);
    }
    public void eliminar() throws Exception{
        
    }
    public void cargar_textos() throws Exception{
        obj_tarea = new modelo_tarea();
        obj_tarea=(modelo_tarea) tabla.getRowData();
        obj_secpoa.setId_secpoa(obj_tarea.getId_secpoa());
        obj_docentes.setPro_codigo(Integer.parseInt(obj_tarea.getProfesor_cod()));
        activa_botones(true, false);
    }
    
    public void activa_botones(boolean guardar,boolean actualizar){
        this.guardar=guardar;
        this.actualizar=actualizar;
    }

    public modelo_docente getObj_docentes() {
        return obj_docentes;
    }

    public void setObj_docentes(modelo_docente obj_docentes) {
        this.obj_docentes = obj_docentes;
    }

    public dao_interfaz_docente getObj_interfaz_docente() {
        return obj_interfaz_docente;
    }

    public void setObj_interfaz_docente(dao_interfaz_docente obj_interfaz_docente) {
        this.obj_interfaz_docente = obj_interfaz_docente;
    }

    public List<modelo_docente> getLista_docentes() {
        return lista_docentes;
    }

    public void setLista_docentes(List<modelo_docente> lista_docentes) {
        this.lista_docentes = lista_docentes;
    }
    
    

    public DataTable getTabla() {
        return tabla;
    }

    public void setTabla(DataTable tabla) {
        this.tabla = tabla;
    }

    public boolean isGuardar() {
        return guardar;
    }

    public void setGuardar(boolean guardar) {
        this.guardar = guardar;
    }

    public boolean isActualizar() {
        return actualizar;
    }

    public void setActualizar(boolean actualizar) {
        this.actualizar = actualizar;
    }
    
    

    public modelo_tarea getObj_tarea() {
        return obj_tarea;
    }

    public void setObj_tarea(modelo_tarea obj_tarea) {
        this.obj_tarea = obj_tarea;
    }

    public modelo_seccion_poa getObj_secpoa() {
        return obj_secpoa;
    }

    public void setObj_secpoa(modelo_seccion_poa obj_secpoa) {
        this.obj_secpoa = obj_secpoa;
    }

    public dao_interfaz_seccion_poa getObj_interfaz_secpoa() {
        return obj_interfaz_secpoa;
    }

    public void setObj_interfaz_secpoa(dao_interfaz_seccion_poa obj_interfaz_secpoa) {
        this.obj_interfaz_secpoa = obj_interfaz_secpoa;
    }

    public dao_interfaz_tarea getObj_interfaz_tarea() {
        return obj_interfaz_tarea;
    }

    public void setObj_interfaz_tarea(dao_interfaz_tarea obj_interfaz_tarea) {
        this.obj_interfaz_tarea = obj_interfaz_tarea;
    }

    public List<modelo_seccion_poa> getLista_secpoa() {
        return lista_secpoa;
    }

    public void setLista_secpoa(List<modelo_seccion_poa> lista_secpoa) {
        this.lista_secpoa = lista_secpoa;
    }

    public List<modelo_tarea> getLista_tarea() {
        return lista_tarea;
    }

    public void setLista_tarea(List<modelo_tarea> lista_tarea) {
        this.lista_tarea = lista_tarea;
    }
    
}
