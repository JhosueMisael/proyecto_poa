/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import DAO_Implementa.dao_implementa_poa;
import DAO_Interfaz.dao_interfaz_poa;
import DAO_Implementa.dao_implementa_seccion_poa;
import DAO_Interfaz.dao_interfaz_seccion_poa;
import Modelo.modelo_poa;
import Modelo.modelo_seccion_poa;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author Christian
 */
@ManagedBean(name = "bean_seccion_poa")
@ViewScoped
public class bean_seccion_poa {
    
    private org.primefaces.component.datatable.DataTable tabla;
    
    modelo_seccion_poa obj_seccion_poa;
    modelo_poa obj_poa;
    
    List<modelo_seccion_poa> lista_seccion_poa=new ArrayList<>();
    List<modelo_poa> lista_poa= new ArrayList<>();
    
    dao_interfaz_poa obj_dao_interfaz_poa=new dao_implementa_poa();
    dao_interfaz_seccion_poa obj_dao_interfaz_seccion = new dao_implementa_seccion_poa();
    
    boolean guardar=false;
    boolean actualizar=false;
    
    /**
     * Creates a new instance of bean_seccion_poa
     * @throws java.lang.Exception
     */
    public bean_seccion_poa() throws Exception {
        lista_poa=obj_dao_interfaz_poa.Lista_POA();
        lista_seccion_poa = obj_dao_interfaz_seccion.Lista_Seccion_POA();
        obj_seccion_poa=new modelo_seccion_poa();
        obj_poa=new modelo_poa();
        activa_botones(false, true);
    }
    public void activa_botones(boolean guardar,boolean actualizar){
        this.guardar=guardar;
        this.actualizar=actualizar;
    }

    public boolean isGuardar() {
        return guardar;
    }

    public void setGuardar(boolean guardar) {
        this.guardar = guardar;
    }

    public boolean isActualizar() {
        return actualizar;
    }

    public void setActualizar(boolean actualizar) {
        this.actualizar = actualizar;
    }
    
    

    public modelo_poa getObj_poa() {
        return obj_poa;
    }

    public void setObj_poa(modelo_poa obj_poa) {
        this.obj_poa = obj_poa;
    }
    

    public modelo_seccion_poa getObj_seccion_poa() {
        return obj_seccion_poa;
    }

    public void setObj_seccion_poa(modelo_seccion_poa obj_seccion_poa) {
        this.obj_seccion_poa = obj_seccion_poa;
    }

    public List<modelo_seccion_poa> getLista_seccion_poa() {
        return lista_seccion_poa;
    }

    public void setLista_seccion_poa(List<modelo_seccion_poa> lista_seccion_poa) {
        this.lista_seccion_poa = lista_seccion_poa;
    }

    public dao_interfaz_seccion_poa getObj_dao_interfaz_seccion() {
        return obj_dao_interfaz_seccion;
    }

    public void setObj_dao_interfaz_seccion(dao_interfaz_seccion_poa obj_dao_interfaz_seccion) {
        this.obj_dao_interfaz_seccion = obj_dao_interfaz_seccion;
    }

    
    public List<modelo_poa> getLista_poa() {
        return lista_poa;
    }

    public void setLista_poa(List<modelo_poa> lista_poa) {
        this.lista_poa = lista_poa;
    }

    public dao_interfaz_poa getObj_dao_interfaz_poa() {
        return obj_dao_interfaz_poa;
    }

    public void setObj_dao_interfaz_poa(dao_interfaz_poa obj_dao_interfaz_poa) {
        this.obj_dao_interfaz_poa = obj_dao_interfaz_poa;
    }

    public DataTable getTabla() {
        return tabla;
    }

    public void setTabla(DataTable tabla) {
        this.tabla = tabla;
    }
    
    public void guardar() throws Exception{
        int numero=obj_poa.getId_poa();
        obj_seccion_poa.setId_poa(numero);
        obj_dao_interfaz_seccion.Guardar(obj_seccion_poa);
        lista_seccion_poa = obj_dao_interfaz_seccion.Lista_Seccion_POA();
        obj_seccion_poa= new modelo_seccion_poa();
        obj_poa=new modelo_poa();
        activa_botones(false, true);
    }
    
    public void actualiza() throws Exception{
        int numero=obj_poa.getId_poa();
        obj_seccion_poa.setId_poa(numero);
        obj_dao_interfaz_seccion.Actualizar(obj_seccion_poa);        
        obj_seccion_poa= new modelo_seccion_poa();
        obj_poa=new modelo_poa();
        activa_botones(false, true);
        lista_seccion_poa = obj_dao_interfaz_seccion.Lista_Seccion_POA();
    }
    
    public void eliminar(){
        
    }
    public void cargar_textos() throws Exception{
        obj_poa=new modelo_poa();
        obj_seccion_poa=(modelo_seccion_poa) tabla.getRowData();
        
        obj_poa.setDes_poa(obj_seccion_poa.getDes_poa());
        obj_poa.setId_poa(obj_seccion_poa.getId_poa());
        activa_botones(true, false);
//        System.out.println("++++++++++++++++++++++++++++++++++++++++++"+obj_seccion_poa.toString());
    }
            
}
