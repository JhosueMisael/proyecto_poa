package Bean;

import DAO_Implementa.dao_actividad_respo_imple;
import DAO_Interfaz.dao_actividad_respo_interf;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import Modelo.actividad_responsable;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author Robben10
 */
@Named(value = "bean_actividad_respo")
@SessionScoped
public class bean_actividad_respo implements Serializable {

    private org.primefaces.component.datatable.DataTable tabla_acti_respo;
    dao_actividad_respo_interf obj_dao_actividad_respo = new dao_actividad_respo_imple();
    List<actividad_responsable> lista_actividad_respo = new ArrayList<>();
    actividad_responsable mod_acti_respo;
    boolean estado_btn_guardar = false;
    boolean estado_btn_actualizar = false;

    public bean_actividad_respo() throws Exception {
        lista_actividad_respo = obj_dao_actividad_respo.lista_actividad_respo();
        mod_acti_respo = new actividad_responsable();
        estado_btn_actualizar = true;

    }

    public DataTable getTabla_acti_respo() {
        return tabla_acti_respo;
    }

    public void setTabla_acti_respo(DataTable tabla_acti_respo) {
        this.tabla_acti_respo = tabla_acti_respo;
    }

    public dao_actividad_respo_interf getObj_dao_actividad_respo() {
        return obj_dao_actividad_respo;
    }

    public void setObj_dao_actividad_respo(dao_actividad_respo_interf obj_dao_actividad_respo) {
        this.obj_dao_actividad_respo = obj_dao_actividad_respo;
    }

    public List<actividad_responsable> getLista_actividad_respo() {
        return lista_actividad_respo;
    }

    public void setLista_actividad_respo(List<actividad_responsable> lista_actividad_respo) {
        this.lista_actividad_respo = lista_actividad_respo;
    }

    public actividad_responsable getMod_acti_respo() {
        return mod_acti_respo;
    }

    public void setMod_acti_respo(actividad_responsable mod_acti_respo) {
        this.mod_acti_respo = mod_acti_respo;
    }

    public boolean isEstado_btn_guardar() {
        return estado_btn_guardar;
    }

    public void setEstado_btn_guardar(boolean estado_btn_guardar) {
        this.estado_btn_guardar = estado_btn_guardar;
    }

    public boolean isEstado_btn_actualizar() {
        return estado_btn_actualizar;
    }

    public void setEstado_btn_actualizar(boolean estado_btn_actualizar) {
        this.estado_btn_actualizar = estado_btn_actualizar;
    }

    //////////////////////////////////////////////////////////////////////////
    public void guardar() throws Exception {

        obj_dao_actividad_respo.Guardar(mod_acti_respo);
        lista_actividad_respo = obj_dao_actividad_respo.lista_actividad_respo();
        mod_acti_respo = new actividad_responsable();
        activa_botones(false, true);
    }

    public void actualizar() throws Exception {
        obj_dao_actividad_respo.Actualizar(mod_acti_respo);
        mod_acti_respo = new actividad_responsable();
        activa_botones(false, true);
    }

    public void eliminar() throws Exception {

        mod_acti_respo = new actividad_responsable();
        mod_acti_respo = (actividad_responsable) tabla_acti_respo.getRowData();
        obj_dao_actividad_respo.Eliminar(mod_acti_respo);
        lista_actividad_respo = obj_dao_actividad_respo.lista_actividad_respo();
        activa_botones(false, true);
    }

    public void cargar_textos() throws Exception {
        mod_acti_respo = (actividad_responsable) tabla_acti_respo.getRowData();
        activa_botones(true, false);
    }

    public void activa_botones(boolean estado_btn_guardar, boolean estado_btn_actualizar) {
        this.estado_btn_guardar = estado_btn_guardar;
        this.estado_btn_actualizar = estado_btn_actualizar;
    }

}
