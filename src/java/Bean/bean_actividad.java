package Bean;

import DAO_Implementa.dao_actividad_imple;
import DAO_Interfaz.dao_actividad_interf;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import Modelo.actividad;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author Robben10
 */
@Named(value = "bean_actividad")
@SessionScoped
public class bean_actividad implements Serializable {

    private org.primefaces.component.datatable.DataTable tabla_acti;
    dao_actividad_interf obj_dao_actividad = new dao_actividad_imple();
    List<actividad> lista_actividad = new ArrayList<>();
    actividad mod_acti;
    boolean estado_btn_guardar = false;
    boolean estado_btn_actualizar = false;

    public bean_actividad() throws Exception {

        lista_actividad = obj_dao_actividad.lista_actividad();
        mod_acti = new actividad();
        estado_btn_actualizar = true;

    }

    public DataTable getTabla_acti() {
        return tabla_acti;
    }

    public void setTabla_acti(DataTable tabla_acti) {
        this.tabla_acti = tabla_acti;
    }

    public dao_actividad_interf getObj_dao_actividad() {
        return obj_dao_actividad;
    }

    public void setObj_dao_actividad(dao_actividad_interf obj_dao_actividad) {
        this.obj_dao_actividad = obj_dao_actividad;
    }

    public List<actividad> getLista_actividad() {
        return lista_actividad;
    }

    public void setLista_actividad(List<actividad> lista_actividad) {
        this.lista_actividad = lista_actividad;
    }

    public actividad getMod_acti() {
        return mod_acti;
    }

    public void setMod_acti(actividad mod_acti) {
        this.mod_acti = mod_acti;
    }

    public boolean isEstado_btn_guardar() {
        return estado_btn_guardar;
    }

    public void setEstado_btn_guardar(boolean estado_btn_guardar) {
        this.estado_btn_guardar = estado_btn_guardar;
    }

    public boolean isEstado_btn_actualizar() {
        return estado_btn_actualizar;
    }

    public void setEstado_btn_actualizar(boolean estado_btn_actualizar) {
        this.estado_btn_actualizar = estado_btn_actualizar;
    }

    //////////////////////////////////////////////////////////////////////////////
    public void guardar() throws Exception {

        obj_dao_actividad.Guardar(mod_acti);
        lista_actividad = obj_dao_actividad.lista_actividad();
        mod_acti = new actividad();
        activa_botones(false, true);
    }

    public void actualizar() throws Exception {
        obj_dao_actividad.Actualizar(mod_acti);
        mod_acti = new actividad();
        activa_botones(false, true);
    }

    public void eliminar() throws Exception {

        mod_acti = new actividad();
        mod_acti = (actividad) tabla_acti.getRowData();
        obj_dao_actividad.Eliminar(mod_acti);
        lista_actividad = obj_dao_actividad.lista_actividad();
        activa_botones(false, true);
    }

    public void cargar_textos() throws Exception {
        mod_acti = (actividad) tabla_acti.getRowData();
        activa_botones(true, false);
    }

    public void activa_botones(boolean estado_btn_guardar, boolean estado_btn_actualizar) {
        this.estado_btn_guardar = estado_btn_guardar;
        this.estado_btn_actualizar = estado_btn_actualizar;
    }

}
