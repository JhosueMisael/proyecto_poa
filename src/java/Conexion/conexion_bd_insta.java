/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Christian
 */
public class conexion_bd_insta {
    private String Servidor;
    private String Usuarios;
    private String Clave;
    private String Dase_de_Datos;
    private String Puerto;
    private String Error;

    protected Connection conexion;
    private final String JDBC_DRIVER;
    private final String URL;
    public conexion_bd_insta() {
        this.Servidor = "35.192.7.211";
        this.Usuarios = "ED_CONSULTAS";
        this.Clave = "PG_CONSULTAS";
        this.Puerto = "5432";
        this.Dase_de_Datos = "BDinsta";

        Error = "";
        JDBC_DRIVER = "org.postgresql.Driver";
        URL = "jdbc:postgresql://" + this.Servidor + ":" + this.Puerto + "/" + this.Dase_de_Datos;
    }
    
    public void conectar() throws Exception {
        try {
            Class.forName(JDBC_DRIVER);
            conexion = DriverManager.getConnection(URL, this.Usuarios, this.Clave);
        } catch (Exception e) {
            throw e;
        }
    }

    public void cerrar() throws SQLException {
        if (conexion != null) {
            if (!conexion.isClosed()) {
                conexion.close();
            }
        }
    }
    
}
