package Conexion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 *
 * @author Christian
 */
public class conexion_bd_pg {
     private String Servidor;
    private String Usuarios;
    private String Clave;
    private String Dase_de_Datos;
    private String Puerto;
    private String Error;

    protected Connection conexion;
    private final String JDBC_DRIVER;
    private final String URL;

    public conexion_bd_pg() {
        this.Servidor = "localhost";
        this.Usuarios = "postgres";
        this.Clave = "12345";
        this.Puerto = "5432";
        this.Dase_de_Datos = "poa";

        Error = "";
        JDBC_DRIVER = "org.postgresql.Driver";
        URL = "jdbc:postgresql://" + this.Servidor + ":" + this.Puerto + "/" + this.Dase_de_Datos;
    }

    public void conectar() throws Exception {
        try {
            Class.forName(JDBC_DRIVER);
            conexion = DriverManager.getConnection(URL, this.Usuarios, this.Clave);
        } catch (Exception e) {
            throw e;
        }
    }

    public void cerrar() throws SQLException {
        if (conexion != null) {
            if (!conexion.isClosed()) {
                conexion.close();
            }
        }
    }
}
